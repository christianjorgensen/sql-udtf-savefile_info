# SQL UDTF Savefile Info #

A series of SQL user defined table functions to return information from a savefile.

## What is this repository for? ##

The series of SQL table functions will return different kind of information from a savefile and contains the following functions:

Function                           | Description                                                     | Status
-----------------------------------|:----------------------------------------------------------------|----------
Savefile_Library_Info              | Return information of the library saved into the savefile.
Savefile_Object_Info               | Return information of the objects saved into the savefile.      |
Savefile_Member_Info               | Return information of the members saved into the savefile.      | *(not yet implemented)*
Savefile_Spooled_Files_Info        | Return information of the spooled files saved into the savefile.| *(not yet implemented)*
Savefile_Info                      | Return combined information from the other four functions.      | *(not yet implemented)*



## SQL UDTF Savefile_Library_Info ##

This SQL function will return information about library saved in the savefile.

The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | char(10)                      | Specifies the savefile for which info is to be returned.
Savefile_library                   | char(10)                      | Specifies the library with the savefile. Special values '\*LIBL' (the default) and '\*CURRENT' can be used.

The following attributes are returned:

Attribute                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | varchar(10)                   | The name of the savefile found.
Savefile_library                   | varchar(10)                   | The name of the library containing the savefile found.
Library_saved                      | varchar(10)                   | The name of the library from which the objects are saved.
Save_command                       | varchar(10)                   | The save command that is used when the save operation is performed.
Save_date_and_time                 | timestamp(0)                  | The time at which the objects were saved.
Auxiliary_storage_pool             | integer                       | The auxiliary storage pool (ASP) of the object when it was saved.
Records                            | integer                       | The number of records used to contain the saved information in the save file.
Objects_saved                      | integer                       | The number of objects that are saved for this library.
Access_paths                       | integer                       | The number of logical file access paths that were saved for the library.
Save_active                        | varchar(10)                   | Whether objects in the library are allowed to be updated while they are being saved.
Release_level                      | varchar(6)                    | The earliest release level of the operating system on which the objects can be restored.
Data_compressed                    | varchar(3)                    | YES or NO. Whether the data was stored in compressed format.
System_serial_number               | varchar(8)                    | The serial number of the system on which the save was performed.
Private_authorities                | varchar(3)                    | YES or NO. Whether the save operation specified that private authorities should be saved with the objects.
Auxiliary_storage_pool_device_name | varchar(10)                   | The name of the independent auxiliary storage pool (ASP) device of the library when it was saved.
Members_in_library_saved           | integer                       | The number of members saved for the library.
Spooled_files_saved                | integer                       | The number of spooled files saved in the save file.
Synchronization_ID                 | varchar(10)                   | The name that was used to synchronize checkpoints for more than one save while active operation.



## SQL UDTF Savefile_Object_Info ##

This SQL function will return information about objects saved in the savefile.

The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | char(10)                      | Savefile for which info is to be returned.
Savefile_library                   | char(10)                      | Library with the savefile. Special values '\*LIBL' (the default) and '\*CURRENT' can be used.
Object_name_filter                 | char(10)                      | Name, a generic name, or the special value '\*ALL' (the default).
Object_type_filter                 | char(10)                      | Type of objects to search for or the special value '\*ALL' (the default).

The following attributes are returned:

Attribute                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | varchar(10)                   | The name of the savefile found.
Savefile_library                   | varchar(10)                   | The name of the library containing the savefile found.
Object_name                        | varchar(10)                   | The name of the object saved. If the object is a DLO object, this field will contain the system name of the object.
Library_saved                      | varchar(10)                   | The name of the library from which the objects are saved.
Object_type                        | varchar(10)                   | The type of object. For a list of object types, see the [Control language](https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_73/rbam6/rbam6clmain.htm?view=kc) topic collection.
Extended_object_attribute          | varchar(10)                   | Extended information about the object type.
Save_date_and_time                 | timestamp(0)                  | The time at which the objects were saved in system time-stamp format.
Object_size                        | bigint                        | The size of the object. The true object size is equal to or smaller than the value returned.
Auxiliary_storage_pool             | integer                       | The auxiliary storage pool (ASP) of the object when it was saved.
Data_saved                         | varchar(3)                    | YES or NO. Whether the data for this object was saved with the object.
Object_owner                       | varchar(10)                   | The name of the object owner's user profile.
Document_library_object_DLO_name   | varchar(20)                   | The name of the document, folder, or mail object that was saved.
Folder                             | varchar(63)                   | The name of the folder that was saved. The folder name is a fully qualified name. If the object is not a *FLR or *DOC object, this field will be blank.
Text_description                   | varchar(50)                   | The text description of the object.
Auxiliary_storage_pool_device_name | varchar(10)                   | The name of the independent auxiliary storage pool (ASP) device of the library when it was saved.



## SQL UDTF Savefile_Member_Info ##

This SQL function will return information about members in a file saved in the savefile.

The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | char(10)                      | Savefile for which info is to be returned.
Savefile_library                   | char(10)                      | Library with the savefile. Special values '\*LIBL' (the default) and '\*CURRENT' can be used.
Object_name_filter                 | char(10)                      | Name, a generic name, or the special value '\*ALL' (the default).

The following attributes are returned:

Attribute                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | varchar(10)                   | The name of the savefile found.
Savefile_library                   | varchar(10)                   | The name of the library containing the savefile found.
File_name                          | varchar(10)                   | The name of the file saved.
Library_saved                      | varchar(10)                   | The name of the library from which the file is saved.
Member_name                        | varchar(10)                   | The name of the file member that is saved. The member names are not in sorted order.
Extended_object_attribute          | varchar(10)                   | Extended information about the file type.
Save_date_and_time                 | timestamp(0)                  | The time at which the members were saved in system time-stamp format.
Members_saved                      | bigint                        | The number of members saved for the file.



## SQL UDTF Savefile_Spooled_Files_Info ##

This SQL function will return information about spooled files in a output queue saved in the savefile.

The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | char(10)                      | Savefile for which info is to be returned.
Savefile_library                   | char(10)                      | Library with the savefile. Special values '\*LIBL' (the default) and '\*CURRENT' can be used.
Object_name_filter                 | char(10)                      | Name, a generic name, or the special value '\*ALL' (the default).

The following attributes are returned:

Attribute                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | varchar(10)                   | The name of the savefile found.
Savefile_library                   | varchar(10)                   | The name of the library containing the savefile found.
Job_name                           | varchar(10)                   | The name of the job that owns the spooled file.
User_name                          | varchar(10)                   | The name of the user who owns the spooled file.
Job_number                         | varchar(6)                    | The number of the job that owns the spooled file.
Spooled_file_name                  | varchar(10)                   | The name of the spooled file.
Spooled_file_number                | integer                       | The number of the spooled file in the job that owns it.
Job_system_name                    | varchar(8)                    | The name of the system where the job that owns the spooled file ran.
Creation_date                      | date                          | The date the spooled file was created.
Creation_time                      | time                          | The time the spooled file was created
Output_queue_name                  | varchar(10)                   | The name of the output queue that contained the spooled file.
Output_queue_library               | varchar(10)                   | The name of the output queue library that contained the spooled file.



## SQL UDTF Savefile_Info ##

This SQL function will return information from all the functions above and combine the result.

Please note the following

* Only unique columns are returned, e.g. Auxiliary_storage_pool is not duplicated.
* Columns not valid for an object type will have null values, e.g. the columns from UDTF Savefile_Spooled_Files_Info are all null, when the object is not an output queue.
* It is always better to be as specific as possible and call the functions separately. This function calls all the functions above, even though no output is returned from the function.

The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Savefile                           | char(10)                      | Savefile for which info is to be returned.
Savefile_library                   | char(10)                      | Library with the savefile. Special values '\*LIBL' (the default) and '\*CURRENT' can be used.
Object_name_filter                 | char(10)                      | Name, a generic name, or the special value '\*ALL' (the default).
Object_type_filter                 | char(10)                      | Type of objects to search for or the special value '\*ALL' (the default).

The attributes from each of the functions above are returned. See the descriptions above for more detail.



## How do I get set up? ##

Clone this git repository to a local directory in the IFS, e.g. in your home directory.
Compile the source using the following CL commands (objects will be placed in the QGPL library):

```
CRTSQLRPGI OBJ(QGPL/SAVFINF) SRCSTMF('savfinf.sqlrpgle') OBJTYPE(*MODULE)
CRTSRVPGM  SRVPGM(QGPL/SAVFINF) EXPORT(*ALL) TEXT('Savefile_Info UDTF''s')
RUNSQLSTM  SRCSTMF('udtf_Savefile_Info.sql') DFTRDBCOL(QGPL)
```

Call the SQL Savefile_Library_Info function like the following
```
select * from table( qgpl.savefile_library_info( 'savfname', 'qgpl' ) )
```

or without a library (library list is used to find the savefile):
```
select * from table( qgpl.savefile_library_info( 'savfname' ) )
```



## Documentation ##

[List Save File (QSRLSAVF) API](https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_73/apis/qsrlsavf.htm)
