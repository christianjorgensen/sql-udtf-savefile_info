**free
/if not defined( Copying_prototypes )
//-----------------------------------------------------------------------------------------------------------/
//                                                                                                           /
//  Brief description of collection of procedures.                                                           /
//                                                                                                           /
//  Procedures:                                                                                              /
//                                                                                                           /
//  Savefile_Library_Info        - return library info from a savefile.                                      /
//  Savefile_Object_Info         - return object info from a savefile.                                       /
//  Savefile_Member_Info         - return member info from a savefile.                                       /
//  Savefile_Spooled_Files_Info  - return spooled files info from a savefile.                                /
//                                                                                                           /
//  Compilation:                                                                                             /
//                                                                                                           /
//*>  CRTSQLRPGI OBJ(&FCN2/&FNR) SRCSTMF('&FP') OBJTYPE(*MODULE)                                           <*/
//*>  CRTSRVPGM  SRVPGM(&FCN2/&FNR) EXPORT(*ALL) TEXT('Savefile Info UDTF''s')                             <*/
//*>  DLTMOD     MODULE(&FCN2/&FNR)                                                                        <*/
//                                                                                                           /
//-----------------------------------------------------------------------------------------------------------/
//  2019-07-02 : Christian Jorgensen                                                                         /
//               Create module.                                                                              /
//  2019-09-25 : Christian Jorgensen                                                                         /
//               Fix typo in parameter name in prototype for Savefile_Library_Info.                          /
//  2020-02-05 : Christian Jorgensen                                                                         /
//               Fix cleanup code.                                                                           /
//-----------------------------------------------------------------------------------------------------------/

ctl-opt text  ( 'Savefile Info UDTF''s' );
ctl-opt debug;
ctl-opt option( *Srcstmt : *NoDebugIO );
ctl-opt thread( *serialize );

ctl-opt nomain;

//-----------------------------------------------------------------------------------------------------------/
// Exported procedures:

/endif

/if not defined( SAVFINF_prototype_copied )

// Procedure:    Savefile_Library_Info
// Description:  Return library info from a savefile.

dcl-pr Savefile_Library_Info extproc( *dclcase );
  // Incoming parameters
  p_InSavefile                                           char( 10 ) const;
  p_InLibrary                                            char( 10 ) const;
  // Returned parameters
  p_Savefile                                             varchar( 10 );
  p_Savefile_Library                                     varchar( 10 );
  p_Library_saved                                        varchar( 10 );
  p_Save_command                                         varchar( 10 );
  p_Save_date_and_time                                   timestamp( 0 );
  p_Auxiliary_storage_pool                               int( 10 );
  p_Records                                              int( 10 );
  p_Objects_saved                                        int( 10 );
  p_Access_paths                                         int( 10 );
  p_Save_active                                          varchar( 10 );
  p_Release_level                                        varchar(  6 );
  p_Data_compressed                                      varchar(  3 );
  p_System_serial_number                                 varchar(  8 );
  p_Private_authorities                                  varchar(  3 );
  p_Auxiliary_storage_pool_device_name                   varchar( 10 );
  p_Members_in_library_saved                             int( 10 );
  p_Spooled_files_saved                                  int( 10 );
  p_Synchronization_ID                                   varchar( 10 );
  // Null indicators for incoming parameters
  n_InSavefile                                           int( 5 ) const;
  n_InLibrary                                            int( 5 ) const;
  // Null indicators for returned parameters
  n_Savefile                                             int( 5 );
  n_Savefile_Library                                     int( 5 );
  n_Library_saved                                        int( 5 );
  n_Save_command                                         int( 5 );
  n_Save_date_and_time                                   int( 5 );
  n_Auxiliary_storage_pool                               int( 5 );
  n_Records                                              int( 5 );
  n_Objects_saved                                        int( 5 );
  n_Access_paths                                         int( 5 );
  n_Save_active                                          int( 5 );
  n_Release_level                                        int( 5 );
  n_Data_compressed                                      int( 5 );
  n_System_serial_number                                 int( 5 );
  n_Private_authorities                                  int( 5 );
  n_Auxiliary_storage_pool_device_name                   int( 5 );
  n_Members_in_library_saved                             int( 5 );
  n_Spooled_files_saved                                  int( 5 );
  n_Synchronization_ID                                   int( 5 );
  // SQL parameters.
  Sql_State   char( 5 );
  Function    varchar( 517 ) const;
  Specific    varchar( 128 ) const;
  MsgText     varchar( 1000 );
  CallType    int( 10 )      const;
end-pr;

// Procedure:    Savefile_Object_Info
// Description:  Return object info from a savefile.

dcl-pr Savefile_Object_Info extproc( *dclcase );
  // Incoming parameters
  p_InSavefile                                           char( 10 ) const;
  p_InLibrary                                            char( 10 ) const;
  p_InObjNameFilter                                      char( 10 ) const;
  p_InObjTypeFilter                                      char( 10 ) const;
  // Returned parameters
  p_Savefile                                             varchar( 10 );
  p_Savefile_Library                                     varchar( 10 );
  p_Object_name                                          varchar( 10 );
  p_Library_saved                                        varchar( 10 );
  p_Object_type                                          varchar( 10 );
  p_Extended_object_attribute                            varchar( 10 );
  p_Save_date_and_time                                   timestamp(0);
  p_Object_size                                          int( 20 );
  p_Auxiliary_storage_pool                               int( 10 );
  p_Data_saved                                           varchar(  3 );
  p_Object_owner                                         varchar( 10 );
  p_Document_library_object_DLO_name                     varchar( 20 );
  p_Folder                                               varchar( 63 );
  p_Text_description                                     varchar( 50 );
  p_Auxiliary_storage_pool_device_name                   varchar( 10 );
  // Null indicators for incoming parameters
  n_InSavefile                                           int( 5 ) const;
  n_InLibrary                                            int( 5 ) const;
  n_InObjNameFilter                                      int( 5 ) const;
  n_InObjTypeFilter                                      int( 5 ) const;
  // Null indicators for returned parameters
  n_Savefile                                             int( 5 );
  n_Savefile_Library                                     int( 5 );
  n_Object_name                                          int( 5 );
  n_Library_saved                                        int( 5 );
  n_Object_type                                          int( 5 );
  n_Extended_object_attribute                            int( 5 );
  n_Save_date_and_time                                   int( 5 );
  n_Object_size                                          int( 5 );
  n_Auxiliary_storage_pool                               int( 5 );
  n_Data_saved                                           int( 5 );
  n_Object_owner                                         int( 5 );
  n_Document_library_object_DLO_name                     int( 5 );
  n_Folder                                               int( 5 );
  n_Text_description                                     int( 5 );
  n_Auxiliary_storage_pool_device_name                   int( 5 );
  // SQL parameters.
  Sql_State   char( 5 );
  Function    varchar( 517 ) const;
  Specific    varchar( 128 ) const;
  MsgText     varchar( 1000 );
  CallType    int( 10 )      const;
end-pr;

// Procedure:    Savefile_Member_Info
// Description:  Return member info from a savefile.

dcl-pr Savefile_Member_Info extproc( *dclcase );
  // Incoming parameters
  p_InSavefile                                           char( 10 ) const;
  p_InLibrary                                            char( 10 ) const;
  p_InObjNameFilter                                      char( 10 ) const;
  // Returned parameters
  p_Savefile                                             varchar( 10 );
  p_Savefile_Library                                     varchar( 10 );
  p_File_name                                            varchar( 10 );
  p_Library_saved                                        varchar( 10 );
  p_Member_name                                          varchar( 10 );
  p_Extended_object_attribute                            varchar( 10 );
  p_Save_date_and_time                                   timestamp(0);
  p_Members_saved                                        int( 10 );
  // Null indicators for incoming parameters
  n_InSavefile                                           int( 5 ) const;
  n_InLibrary                                            int( 5 ) const;
  n_InObjNameFilter                                      int( 5 ) const;
  // Null indicators for returned parameters
  n_Savefile                                             int( 5 );
  n_Savefile_Library                                     int( 5 );
  n_File_name                                            int( 5 );
  n_Library_saved                                        int( 5 );
  n_Member_name                                          int( 5 );
  n_Extended_object_attribute                            int( 5 );
  n_Save_date_and_time                                   int( 5 );
  n_Members_saved                                        int( 5 );
  // SQL parameters.
  Sql_State   char( 5 );
  Function    varchar( 517 ) const;
  Specific    varchar( 128 ) const;
  MsgText     varchar( 1000 );
  CallType    int( 10 )      const;
end-pr;

// Procedure:    Savefile_Spooled_Files_Info
// Description:  Return spooled files info from a savefile.

dcl-pr Savefile_Spooled_Files_Info extproc( *dclcase );
  // Incoming parameters
  p_InSavefile                                           char( 10 ) const;
  p_InLibrary                                            char( 10 ) const;
  p_InObjNameFilter                                      char( 10 ) const;
  // Returned parameters
  p_Savefile                                             varchar( 10 );
  p_Savefile_Library                                     varchar( 10 );
  p_Job_name                                             varchar( 10 );
  p_User_name                                            varchar( 10 );
  p_Job_number                                           varchar(  6 );
  p_Spooled_file_name                                    varchar( 10 );
  p_Spooled_file_number                                  int( 10 );
  p_Job_system_name                                      varchar(  8 );
  p_Creation_date                                        date;
  p_Creation_time                                        time;
  p_Output_queue_name                                    varchar( 10 );
  p_Output_queue_library                                 varchar( 10 );
  // Null indicators for incoming parameters
  n_InSavefile                                           int( 5 ) const;
  n_InLibrary                                            int( 5 ) const;
  n_InObjNameFilter                                      int( 5 ) const;
  // Null indicators for returned parameters
  n_Savefile                                             int( 5 );
  n_Savefile_Library                                     int( 5 );
  n_Job_name                                             int( 5 );
  n_User_name                                            int( 5 );
  n_Job_number                                           int( 5 );
  n_Spooled_file_name                                    int( 5 );
  n_Spooled_file_number                                  int( 5 );
  n_Job_system_name                                      int( 5 );
  n_Creation_date                                        int( 5 );
  n_Creation_time                                        int( 5 );
  n_Output_queue_name                                    int( 5 );
  n_Output_queue_library                                 int( 5 );
  // SQL parameters.
  Sql_State   char( 5 );
  Function    varchar( 517 ) const;
  Specific    varchar( 128 ) const;
  MsgText     varchar( 1000 );
  CallType    int( 10 )      const;
end-pr;

//-----------------------------------------------------------------------------------------------------------/
// Exported data:

/define SAVFINF_prototype_copied
/if defined( Copying_prototypes )
/eof

/endif
/endif

//-----------------------------------------------------------------------------------------------------------/
// Constants:

dcl-c CALL_STARTUP    -2;
dcl-c CALL_OPEN       -1;
dcl-c CALL_FETCH       0;
dcl-c CALL_CLOSE       1;
dcl-c CALL_FINAL       2;

dcl-c PARM_NULL       -1;
dcl-c PARM_NOTNULL     0;

dcl-c TEMP_USERSPACE      'SAVFINF';
dcl-c TEMP_USERSPACE_LIB  'QTEMP';

//-----------------------------------------------------------------------------------------------------------/
// Data types:

dcl-ds QualObj_t template qualified;
  Obj     char( 10 );
  Lib     char( 10 );
end-ds;

// HEADER - savefile list header information.

dcl-ds HEADER_t template qualified;
  User_space_name_used                                          char( 10 );
  User_space_library_name_used                                  char( 10 );
  Save_file_name_used                                           char( 10 );
  Save_file_library_name_used                                   char( 10 );
  Continuation_handle_returned                                  char( 36 );
end-ds;

// SAVF0100 - library level information.

dcl-ds SAVF0100_t template qualified;
  Library_saved                                                 char( 10 );
  Save_command                                                  char( 10 );
  Save_date_and_time                                            char(  8 );
  Auxiliary_storage_pool                                        int( 10 );
  Records                                                       uns( 10 );
  Objects_saved                                                 int( 10 );
  Access_paths                                                  int( 10 );
  Save_active                                                   char( 10 );
  Release_level                                                 char(  6 );
  Data_compressed                                               char(  1 );
  System_serial_number                                          char(  8 );
  Private_authorities                                           char(  1 );
  Reserved                                                      char(  2 );
  Auxiliary_storage_pool_device_name                            char( 10 );
  *n                                                            char(  2 );
  Members_in_library_saved                                      int( 10 );
  Spooled_files_saved                                           int( 10 );
  Synchronization_ID                                            char( 10 );
end-ds;

// SAVF0200 - object level information.

dcl-ds SAVF0200_t template qualified;
  Object_name                                                   char( 10 );
  Library_saved                                                 char( 10 );
  Object_type                                                   char( 10 );
  Extended_object_attribute                                     char( 10 );
  Save_date_and_time                                            char(  8 );
  Object_size                                                   int( 10 );
  Object_size_multiplier                                        int( 10 );
  Auxiliary_storage_pool                                        int( 10 );
  Data_saved                                                    char(  1 );
  Object_owner                                                  char( 10 );
  Document_library_object_DLO_name                              char( 20 );
  Folder                                                        char( 63 );
  Text_description                                              char( 50 );
  Auxiliary_storage_pool_device_name                            char( 10 );
end-ds;

// SAVF0300 - member level information.

dcl-ds SAVF0300_t template qualified;
  File_name                                                     char( 10 );
  Library_saved                                                 char( 10 );
  Member_name                                                   char( 10 );
  Extended_object_attribute                                     char( 10 );
  Save_date_and_time                                            char(  8 );
  Members_saved                                                 int( 10 );
end-ds;

// SAVF0400 - spooled files information.

dcl-ds SAVF0400_t template qualified;
  Job_name                                                      char( 10 );
  User_name                                                     char( 10 );
  Job_number                                                    char(  6 );
  Spooled_file_name                                             char( 10 );
  Spooled_file_number                                           int( 10 );
  Job_system_name                                               char(  8 );
  Creation_date                                                 char(  7 );
  Creation_time                                                 char(  6 );
  Output_queue_name                                             char( 10 );
  Output_queue_library                                          char( 10 );
  *n                                                            char(  3 );
end-ds;

// User space generic header
dcl-ds UsrSpcHdr_t template qualified;
  OfsInpSec  int( 10 ) pos( 109 );
  SizInpSec  int( 10 ) pos( 113 );
  OfsHdrSec  int( 10 ) pos( 117 );
  SizHdrSec  int( 10 ) pos( 121 );
  OfsLstEnt  int( 10 ) pos( 125 );
  NumLstEnt  int( 10 ) pos( 133 );
  SizLstEnt  int( 10 ) pos( 137 );
end-ds;

dcl-s  Sys_timestamp_t char( 8 ) template;

// API error data structure:

dcl-ds ApiError_t template qualified;
  BytPrv  int( 10 )   inz( %size( ApiError_t ) );
  BytAvl  int( 10 );
  ExcpId  char(   7 );
  *n      char(   1 );
  ExcpDta char( 256 );
end-ds;

//-----------------------------------------------------------------------------------------------------------/
// Global data:

// Change user space to auto extend:

dcl-ds AttrsToChg qualified;
  NumRecs   int( 10 ) inz( 1 );
  Key       int( 10 ) inz( 3 );
  DtaLen    int( 10 ) inz( 1 );
  Extend    char( 1 ) inz( '1' ) ;
end-ds;

//-----------------------------------------------------------------------------------------------------------/
// Prototypes:

/define Copying_prototypes

/undefine Copying_prototypes

dcl-pr List_Savefile_Information extpgm( 'QSRLSAVF' );
  *n like( QualObj_t ) const;                              // Qualified User Space Name
  *n char(    8 )      const;                              // Format Name
  *n like( QualObj_t ) const;                              // Qualified savefile name
  *n char(   10 )      const;                              // Object name filter
  *n char(   10 )      const;                              // Object type filter
  *n char(   36 )      const;                              // Continuation handle
  *n char( 1024 )      const options( *varsize );          // Error code
end-pr;

// Create user space
dcl-pr CrtUsrSpc extpgm( 'QUSCRTUS' );
  SpcNam_q  char(    20 ) const;
  ExtAtr    char(    10 ) const;
  InzSiz    int( 10 )     const;
  InzVal    char(     1 ) const;
  PubAut    char(    10 ) const;
  Text      char(    50 ) const;
  Replace   char(    10 ) const;
  *n        char(  1024 ) const options( *varsize );       // Error code
  Domain    char(    10 ) const options( *nopass );
end-pr;

// Change user space attributes
dcl-pr ChgUsrSpcAttr extpgm( 'QUSCUSAT' );
  OutLib    char(    10 );
  SpcNam_q  char(    20 ) const;
  ChgAttr   char( 32767 )       options( *varsize );
  Error     char( 32767 ) const options( *nopass : *varsize );
end-pr;

// Delete user space
dcl-pr DltUsrSpc extpgm( 'QUSDLTUS' );
  SpcNam_q  char(    20 ) const;
  Error     char( 32767 ) const options( *nopass : *varsize );
end-pr;

// Get pointer to user space
dcl-pr GetPtrUsrSpc extpgm( 'QUSPTRUS' );
  SpcNam_q  char(    20 ) const;
  Pointer   pointer;
  Error     char( 32767 ) const options( *nopass : *varsize );
end-pr;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    Savefile_Library_Info
// Description:  Return library info from a savefile.

dcl-proc Savefile_Library_Info export;

  dcl-pi *n;
    // Incoming parameters
    p_InSavefile                                           char( 10 ) const;
    p_InLibrary                                            char( 10 ) const;
    // Returned parameters
    p_Savefile                                             varchar( 10 );
    p_Savefile_Library                                     varchar( 10 );
    p_Library_saved                                        varchar( 10 );
    p_Save_command                                         varchar( 10 );
    p_Save_date_and_time                                   timestamp( 0 );
    p_Auxiliary_storage_pool                               int( 10 );
    p_Records                                              int( 10 );
    p_Objects_saved                                        int( 10 );
    p_Access_paths                                         int( 10 );
    p_Save_active                                          varchar( 10 );
    p_Release_level                                        varchar(  6 );
    p_Data_compressed                                      varchar(  3 );
    p_System_serial_number                                 varchar(  8 );
    p_Private_authorities                                  varchar(  3 );
    p_Auxiliary_storage_pool_device_name                   varchar( 10 );
    p_Members_in_library_saved                             int( 10 );
    p_Spooled_files_saved                                  int( 10 );
    p_Synchronization_ID                                   varchar( 10 );
    // Null indicators for incoming parameters
    n_InSavefile                                           int( 5 ) const;
    n_InLibrary                                            int( 5 ) const;
    // Null indicators for returned parameters
    n_Savefile                                             int( 5 );
    n_Savefile_Library                                     int( 5 );
    n_Library_saved                                        int( 5 );
    n_Save_command                                         int( 5 );
    n_Save_date_and_time                                   int( 5 );
    n_Auxiliary_storage_pool                               int( 5 );
    n_Records                                              int( 5 );
    n_Objects_saved                                        int( 5 );
    n_Access_paths                                         int( 5 );
    n_Save_active                                          int( 5 );
    n_Release_level                                        int( 5 );
    n_Data_compressed                                      int( 5 );
    n_System_serial_number                                 int( 5 );
    n_Private_authorities                                  int( 5 );
    n_Auxiliary_storage_pool_device_name                   int( 5 );
    n_Members_in_library_saved                             int( 5 );
    n_Spooled_files_saved                                  int( 5 );
    n_Synchronization_ID                                   int( 5 );
    // SQL parameters.
    Sql_State   char( 5 );
    Function    varchar( 517 ) const;
    Specific    varchar( 128 ) const;
    MsgText     varchar( 1000 );
    CallType    int( 10 )      const;
  end-pi;

  dcl-s  InSavefile        like( p_InSavefile );
  dcl-s  InLibrary         like( p_InLibrary );

  // User space and pointers
  dcl-ds QualUsrSpc        likeds( QualObj_t ) static;
  dcl-s  ptrUsrSpc         pointer static inz( *null );
  dcl-s  ptrInpInf         pointer static inz( *null );
  dcl-s  ptrHdrInf         pointer static inz( *null );
  dcl-s  ptrLstEnt         pointer static inz( *null );
  dcl-ds UsrSpcHdr         likeds( UsrSpcHdr_t ) based( ptrUsrSpc );

  dcl-ds HEADER            likeds( HEADER_t ) based( ptrHdrInf );
  dcl-ds SAVF0100          likeds( SAVF0100_t ) based( ptrLstEnt );

  dcl-s  CurEntry          like( UsrSpcHdr.NumLstEnt ) static;

  dcl-ds ApiError          likeds( ApiError_t );

  // Global procedure monitor.

  monitor;

    // Start all fields at not NULL.

    n_Savefile                                             = PARM_NOTNULL;
    n_Savefile_Library                                     = PARM_NOTNULL;
    n_Library_saved                                        = PARM_NOTNULL;
    n_Save_command                                         = PARM_NOTNULL;
    n_Save_date_and_time                                   = PARM_NOTNULL;
    n_Auxiliary_storage_pool                               = PARM_NOTNULL;
    n_Records                                              = PARM_NOTNULL;
    n_Objects_saved                                        = PARM_NOTNULL;
    n_Access_paths                                         = PARM_NOTNULL;
    n_Save_active                                          = PARM_NOTNULL;
    n_Release_level                                        = PARM_NOTNULL;
    n_Data_compressed                                      = PARM_NOTNULL;
    n_System_serial_number                                 = PARM_NOTNULL;
    n_Private_authorities                                  = PARM_NOTNULL;
    n_Auxiliary_storage_pool_device_name                   = PARM_NOTNULL;
    n_Members_in_library_saved                             = PARM_NOTNULL;
    n_Spooled_files_saved                                  = PARM_NOTNULL;
    n_Synchronization_ID                                   = PARM_NOTNULL;

    //  Open, fetch & close...

    select;
      when ( CallType = CALL_OPEN );

        exec SQL values upper( :p_InSavefile ) into :InSavefile;
        exec SQL values upper( :p_InLibrary ) into :InLibrary;

        if ( ( n_InSavefile = PARM_NULL ) or ( InSavefile = '' ) );
          SQL_State = '38999';
          MsgText = 'Savefile must be specified.';
          return;
        endif;

        if ( ( n_InLibrary = PARM_NULL ) or ( InLibrary = '' ) );
          SQL_State = '38999';
          MsgText = 'Library must be specified.';
          return;
        endif;

        // Create temporary user space for program info list.

        QualUsrSpc.Obj = TEMP_USERSPACE + '1';
        QualUsrSpc.Lib = TEMP_USERSPACE_LIB;
        CrtUsrSpc( QualUsrSpc
                 : ''
                 : 65535
                 : x'00'
                 : '*CHANGE'
                 : 'Savefile_Library_Info'
                 : '*YES'
                 : x'00000000'
                 );
        ChgUsrSpcAttr( QualUsrSpc.Lib
                     : QualUsrSpc
                     : AttrsToChg
                     : x'00000000'
                     );

        // Get Savefile library info into user space.

        List_Savefile_Information( QualUsrSpc : 'SAVF0100' : InSavefile + InLibrary : '*ALL' : '*ALL' : '' : x'00000000' );

        GetPtrUsrSpc( QualUsrSpc : ptrUsrSpc );

        ptrInpInf = ptrUsrSpc + UsrSpcHdr.OfsInpSec;
        ptrHdrInf = ptrUsrSpc + UsrSpcHdr.OfsHdrSec;
        ptrLstEnt = ptrUsrSpc + UsrSpcHdr.OfsLstEnt;

        // Reset current entry before fetching.

        CurEntry = 0;

      when ( CallType = CALL_FETCH );

        // Read next list entry.

        select;
          when ( CurEntry < UsrSpcHdr.NumLstEnt );
            CurEntry += 1;

            // Copy savefile library info to parameters.

            p_Savefile                                  = %trimr( HEADER.Save_file_name_used );
            p_Savefile_Library                          = %trimr( HEADER.Save_file_library_name_used );
            p_Library_saved                             = %trimr( SAVF0100.Library_saved );
            p_Save_command                              = %trimr( SAVF0100.Save_command );

            // Convert date and time values into timestamps. If error, set parameter to null.
            monitor;
              p_Save_date_and_time = RtvTimestamp( SAVF0100.Save_date_and_time );
            on-error *ALL;
              n_Save_date_and_time = PARM_NULL;
            endmon;

            p_Auxiliary_storage_pool                    = SAVF0100.Auxiliary_storage_pool;
            p_Records                                   = SAVF0100.Records;
            p_Objects_saved                             = SAVF0100.Objects_saved;
            p_Access_paths                              = SAVF0100.Access_paths;
            p_Save_active                               = %trimr( SAVF0100.Save_active );
            p_Release_level                             = %trimr( SAVF0100.Release_level );

            select;
              when ( SAVF0100.Data_compressed = '0' );
                p_Data_compressed = 'NO';
              when ( SAVF0100.Data_compressed = '1' );
                p_Data_compressed = 'YES';
            endsl;

            p_System_serial_number                      = %trim( SAVF0100.System_serial_number );

            select;
              when ( SAVF0100.Private_authorities = '0' );
                p_Private_authorities = 'NO';
              when ( SAVF0100.Private_authorities = '1' );
                p_Private_authorities = 'YES';
            endsl;

            p_Auxiliary_storage_pool_device_name        = %trimr( SAVF0100.Auxiliary_storage_pool_device_name );
            p_Members_in_library_saved                  = SAVF0100.Members_in_library_saved;
            p_Spooled_files_saved                       = SAVF0100.Spooled_files_saved;
            p_Synchronization_ID                        = %trimr( SAVF0100.Synchronization_ID );

            // Forward pointer to next entry.

            if ( CurEntry < UsrSpcHdr.NumLstEnt );
              ptrLstEnt += UsrSpcHdr.SizLstEnt;
            endif;

          // When last entry returned, signal EOF.

          when ( CurEntry >= UsrSpcHdr.NumLstEnt );
            SQL_State = '02000';
            return;

          other;
            SQL_State = '38999';
            MsgText = 'Unknown error reading savefile library info';
            return;
        endsl;

      when ( CallType = CALL_CLOSE );
        DltUsrSpc( QualUsrSpc : x'00000000' );
    endsl;

    // Normal return.

    return;

  // Error handling...

  on-error *all;
    RcvMsg( ApiError.ExcpID : MsgText );
    SQL_State = '38999';
    MsgText = 'Error ' + ApiError.ExcpID + ': ' + MsgText + '. Please check joblog.';
    return;
  endmon;

end-proc;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    Savefile_Object_Info
// Description:  Return object info from a savefile.

dcl-proc Savefile_Object_Info export;

  dcl-pi *n;
    // Incoming parameters
    p_InSavefile                                           char( 10 ) const;
    p_InLibrary                                            char( 10 ) const;
    p_InObjNameFilter                                      char( 10 ) const;
    p_InObjTypeFilter                                      char( 10 ) const;
    // Returned parameters
    p_Savefile                                             varchar( 10 );
    p_Savefile_Library                                     varchar( 10 );
    p_Object_name                                          varchar( 10 );
    p_Library_saved                                        varchar( 10 );
    p_Object_type                                          varchar( 10 );
    p_Extended_object_attribute                            varchar( 10 );
    p_Save_date_and_time                                   timestamp(0);
    p_Object_size                                          int( 20 );
    p_Auxiliary_storage_pool                               int( 10 );
    p_Data_saved                                           varchar(  3 );
    p_Object_owner                                         varchar( 10 );
    p_Document_library_object_DLO_name                     varchar( 20 );
    p_Folder                                               varchar( 63 );
    p_Text_description                                     varchar( 50 );
    p_Auxiliary_storage_pool_device_name                   varchar( 10 );
    // Null indicators for incoming parameters
    n_InSavefile                                           int( 5 ) const;
    n_InLibrary                                            int( 5 ) const;
    n_InObjNameFilter                                      int( 5 ) const;
    n_InObjTypeFilter                                      int( 5 ) const;
    // Null indicators for returned parameters
    n_Savefile                                             int( 5 );
    n_Savefile_Library                                     int( 5 );
    n_Object_name                                          int( 5 );
    n_Library_saved                                        int( 5 );
    n_Object_type                                          int( 5 );
    n_Extended_object_attribute                            int( 5 );
    n_Save_date_and_time                                   int( 5 );
    n_Object_size                                          int( 5 );
    n_Auxiliary_storage_pool                               int( 5 );
    n_Data_saved                                           int( 5 );
    n_Object_owner                                         int( 5 );
    n_Document_library_object_DLO_name                     int( 5 );
    n_Folder                                               int( 5 );
    n_Text_description                                     int( 5 );
    n_Auxiliary_storage_pool_device_name                   int( 5 );
    // SQL parameters.
    Sql_State   char( 5 );
    Function    varchar( 517 ) const;
    Specific    varchar( 128 ) const;
    MsgText     varchar( 1000 );
    CallType    int( 10 )      const;
  end-pi;

  dcl-s  InSavefile        like( p_InSavefile );
  dcl-s  InLibrary         like( p_InLibrary );
  dcl-s  InObjNameFilter   like( p_InObjNameFilter );
  dcl-s  InObjTypeFilter   like( p_InObjTypeFilter );

  // User space and pointers
  dcl-ds QualUsrSpc        likeds( QualObj_t ) static;
  dcl-s  ptrUsrSpc         pointer static inz( *null );
  dcl-s  ptrInpInf         pointer static inz( *null );
  dcl-s  ptrHdrInf         pointer static inz( *null );
  dcl-s  ptrLstEnt         pointer static inz( *null );
  dcl-ds UsrSpcHdr         likeds( UsrSpcHdr_t ) based( ptrUsrSpc );

  dcl-ds HEADER            likeds( HEADER_t ) based( ptrHdrInf );
  dcl-ds SAVF0200          likeds( SAVF0200_t ) based( ptrLstEnt );

  dcl-s  CurEntry          like( UsrSpcHdr.NumLstEnt ) static;

  dcl-ds ApiError          likeds( ApiError_t );

  // Global procedure monitor.

  monitor;

    // Start all fields at not NULL.

    n_Savefile                                             = PARM_NOTNULL;
    n_Savefile_Library                                     = PARM_NOTNULL;
    n_Object_name                                          = PARM_NOTNULL;
    n_Library_saved                                        = PARM_NOTNULL;
    n_Object_type                                          = PARM_NOTNULL;
    n_Extended_object_attribute                            = PARM_NOTNULL;
    n_Save_date_and_time                                   = PARM_NOTNULL;
    n_Object_size                                          = PARM_NOTNULL;
    n_Auxiliary_storage_pool                               = PARM_NOTNULL;
    n_Data_saved                                           = PARM_NOTNULL;
    n_Object_owner                                         = PARM_NOTNULL;
    n_Document_library_object_DLO_name                     = PARM_NOTNULL;
    n_Folder                                               = PARM_NOTNULL;
    n_Text_description                                     = PARM_NOTNULL;
    n_Auxiliary_storage_pool_device_name                   = PARM_NOTNULL;

    //  Open, fetch & close...

    select;
      when ( CallType = CALL_OPEN );

        exec SQL values upper( :p_InSavefile ) into :InSavefile;
        exec SQL values upper( :p_InLibrary ) into :InLibrary;
        exec SQL values upper( :p_InObjNameFilter ) into :InObjNameFilter;
        exec SQL values upper( :p_InObjTypeFilter ) into :InObjTypeFilter;

        if ( ( n_InSavefile = PARM_NULL ) or ( InSavefile = '' ) );
          SQL_State = '38999';
          MsgText = 'Savefile must be specified.';
          return;
        endif;

        if ( ( n_InLibrary = PARM_NULL ) or ( InLibrary = '' ) );
          SQL_State = '38999';
          MsgText = 'Library must be specified.';
          return;
        endif;

        if ( ( n_InObjNameFilter = PARM_NULL ) or ( InObjNameFilter = '' ) );
          SQL_State = '38999';
          MsgText = 'Object name filter must be specified.';
          return;
        endif;

        if ( ( n_InObjTypeFilter = PARM_NULL ) or ( InObjTypeFilter = '' ) );
          SQL_State = '38999';
          MsgText = 'Object type filter must be specified.';
          return;
        endif;

        // Create temporary user space for program info list.

        QualUsrSpc.Obj = TEMP_USERSPACE + '2';
        QualUsrSpc.Lib = TEMP_USERSPACE_LIB;
        CrtUsrSpc( QualUsrSpc
                 : ''
                 : 65535
                 : x'00'
                 : '*CHANGE'
                 : %proc
                 : '*YES'
                 : x'00000000'
                 );
        ChgUsrSpcAttr( QualUsrSpc.Lib
                     : QualUsrSpc
                     : AttrsToChg
                     : x'00000000'
                     );

        // Get Savefile library info into user space.

        List_Savefile_Information( QualUsrSpc
                                 : 'SAVF0200'
                                 : InSavefile + InLibrary
                                 : InObjNameFilter
                                 : InObjTypeFilter
                                 : ''
                                 : x'00000000'
                                 );

        GetPtrUsrSpc( QualUsrSpc : ptrUsrSpc );

        ptrInpInf = ptrUsrSpc + UsrSpcHdr.OfsInpSec;
        ptrHdrInf = ptrUsrSpc + UsrSpcHdr.OfsHdrSec;
        ptrLstEnt = ptrUsrSpc + UsrSpcHdr.OfsLstEnt;

        // Reset current entry before fetching.

        CurEntry = 0;

      when ( CallType = CALL_FETCH );

        // Read next list entry.

        select;
          when ( CurEntry < UsrSpcHdr.NumLstEnt );
            CurEntry += 1;

            // Copy savefile object info to parameters.

            p_Savefile                                  = %trimr( HEADER.Save_file_name_used );
            p_Savefile_Library                          = %trimr( HEADER.Save_file_library_name_used );
            p_Object_name                               = %trimr( SAVF0200.Object_name );
            p_Library_saved                             = %trimr( SAVF0200.Library_saved );
            p_Object_type                               = %trimr( SAVF0200.Object_type );
            p_Extended_object_attribute                 = %trimr( SAVF0200.Extended_object_attribute );

            monitor;
              p_Save_date_and_time = RtvTimestamp( SAVF0200.Save_date_and_time );
            on-error *ALL;
              n_Save_date_and_time = PARM_NULL;
            endmon;

            p_Object_size                               = SAVF0200.Object_size * SAVF0200.Object_size_multiplier;

            p_Auxiliary_storage_pool                    = SAVF0200.Auxiliary_storage_pool;

            select;
              when ( SAVF0200.Data_saved = '0' );
                p_Data_saved = 'NO';
              when ( SAVF0200.Data_saved = '1' );
                p_Data_saved = 'YES';
            endsl;

            p_Object_owner                              = %trimr( SAVF0200.Object_owner );
            p_Document_library_object_DLO_name          = %trimr( SAVF0200.Document_library_object_DLO_name );
            p_Folder                                    = %trimr( SAVF0200.Folder );
            p_Text_description                          = %trimr( SAVF0200.Text_description );
            p_Auxiliary_storage_pool_device_name        = %trimr( SAVF0200.Auxiliary_storage_pool_device_name );

            // Forward pointer to next entry.

            if ( CurEntry < UsrSpcHdr.NumLstEnt );
              ptrLstEnt += UsrSpcHdr.SizLstEnt;
            endif;

          // When last entry returned, signal EOF.

          when ( CurEntry >= UsrSpcHdr.NumLstEnt );
            SQL_State = '02000';
            return;

          other;
            SQL_State = '38999';
            MsgText = 'Unknown error reading savefile object info';
            return;
        endsl;

      when ( CallType = CALL_CLOSE );
        DltUsrSpc( QualUsrSpc : x'00000000' );
    endsl;

    // Normal return.

    return;

  // Error handling...

  on-error *all;
    RcvMsg( ApiError.ExcpID : MsgText );
    SQL_State = '38999';
    MsgText = 'Error ' + ApiError.ExcpID + ': ' + MsgText + '. Please check joblog.';
    return;
  endmon;

end-proc;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    Savefile_Member_Info
// Description:  Return member info from a savefile.

dcl-proc Savefile_Member_Info export;

  dcl-pi *n;
    // Incoming parameters
    p_InSavefile                                           char( 10 ) const;
    p_InLibrary                                            char( 10 ) const;
    p_InObjNameFilter                                      char( 10 ) const;
    // Returned parameters
    p_Savefile                                             varchar( 10 );
    p_Savefile_Library                                     varchar( 10 );
    p_File_name                                            varchar( 10 );
    p_Library_saved                                        varchar( 10 );
    p_Member_name                                          varchar( 10 );
    p_Extended_object_attribute                            varchar( 10 );
    p_Save_date_and_time                                   timestamp(0);
    p_Members_saved                                        int( 10 );
    // Null indicators for incoming parameters
    n_InSavefile                                           int( 5 ) const;
    n_InLibrary                                            int( 5 ) const;
    n_InObjNameFilter                                      int( 5 ) const;
    // Null indicators for returned parameters
    n_Savefile                                             int( 5 );
    n_Savefile_Library                                     int( 5 );
    n_File_name                                            int( 5 );
    n_Library_saved                                        int( 5 );
    n_Member_name                                          int( 5 );
    n_Extended_object_attribute                            int( 5 );
    n_Save_date_and_time                                   int( 5 );
    n_Members_saved                                        int( 5 );
    // SQL parameters.
    Sql_State   char( 5 );
    Function    varchar( 517 ) const;
    Specific    varchar( 128 ) const;
    MsgText     varchar( 1000 );
    CallType    int( 10 )      const;
  end-pi;

  dcl-s  InSavefile        like( p_InSavefile );
  dcl-s  InLibrary         like( p_InLibrary );
  dcl-s  InObjNameFilter   like( p_InObjNameFilter );

  // User space and pointers
  dcl-ds QualUsrSpc        likeds( QualObj_t ) static;
  dcl-s  ptrUsrSpc         pointer static inz( *null );
  dcl-s  ptrInpInf         pointer static inz( *null );
  dcl-s  ptrHdrInf         pointer static inz( *null );
  dcl-s  ptrLstEnt         pointer static inz( *null );
  dcl-ds UsrSpcHdr         likeds( UsrSpcHdr_t ) based( ptrUsrSpc );

  dcl-ds HEADER            likeds( HEADER_t ) based( ptrHdrInf );
  dcl-ds SAVF0300          likeds( SAVF0300_t ) based( ptrLstEnt );

  dcl-s  CurEntry          like( UsrSpcHdr.NumLstEnt ) static;

  dcl-ds ApiError          likeds( ApiError_t );

  // Global procedure monitor.

  monitor;

    // Start all fields at not NULL.

    n_Savefile                                             = PARM_NOTNULL;
    n_Savefile_Library                                     = PARM_NOTNULL;
    n_File_name                                            = PARM_NOTNULL;
    n_Library_saved                                        = PARM_NOTNULL;
    n_Member_name                                          = PARM_NOTNULL;
    n_Extended_object_attribute                            = PARM_NOTNULL;
    n_Save_date_and_time                                   = PARM_NOTNULL;
    n_Members_saved                                        = PARM_NOTNULL;

    //  Open, fetch & close...

    select;
      when ( CallType = CALL_OPEN );

        exec SQL values upper( :p_InSavefile ) into :InSavefile;
        exec SQL values upper( :p_InLibrary ) into :InLibrary;
        exec SQL values upper( :p_InObjNameFilter ) into :InObjNameFilter;

        if ( ( n_InSavefile = PARM_NULL ) or ( InSavefile = '' ) );
          SQL_State = '38999';
          MsgText = 'Savefile must be specified.';
          return;
        endif;

        if ( ( n_InLibrary = PARM_NULL ) or ( InLibrary = '' ) );
          SQL_State = '38999';
          MsgText = 'Library must be specified.';
          return;
        endif;

        if ( ( n_InObjNameFilter = PARM_NULL ) or ( InObjNameFilter = '' ) );
          SQL_State = '38999';
          MsgText = 'Object name filter must be specified.';
          return;
        endif;

        // Create temporary user space for program info list.

        QualUsrSpc.Obj = TEMP_USERSPACE + '3';
        QualUsrSpc.Lib = TEMP_USERSPACE_LIB;
        CrtUsrSpc( QualUsrSpc
                 : ''
                 : 65535
                 : x'00'
                 : '*CHANGE'
                 : %proc
                 : '*YES'
                 : x'00000000'
                 );
        ChgUsrSpcAttr( QualUsrSpc.Lib
                     : QualUsrSpc
                     : AttrsToChg
                     : x'00000000'
                     );

        // Get Savefile library info into user space.

        List_Savefile_Information( QualUsrSpc
                                 : 'SAVF0300'
                                 : InSavefile + InLibrary
                                 : InObjNameFilter
                                 : '*ALL'
                                 : ''
                                 : x'00000000'
                                 );

        GetPtrUsrSpc( QualUsrSpc : ptrUsrSpc );

        ptrInpInf = ptrUsrSpc + UsrSpcHdr.OfsInpSec;
        ptrHdrInf = ptrUsrSpc + UsrSpcHdr.OfsHdrSec;
        ptrLstEnt = ptrUsrSpc + UsrSpcHdr.OfsLstEnt;

        // Reset current entry before fetching.

        CurEntry = 0;

      when ( CallType = CALL_FETCH );

        // Read next list entry.

        select;
          when ( CurEntry < UsrSpcHdr.NumLstEnt );
            CurEntry += 1;

            // Copy savefile member info to parameters.

            p_Savefile                                  = %trimr( HEADER.Save_file_name_used );
            p_Savefile_Library                          = %trimr( HEADER.Save_file_library_name_used );
            p_File_name                                 = %trimr( SAVF0300.File_name );
            p_Library_saved                             = %trimr( SAVF0300.Library_saved );
            p_Member_name                               = %trimr( SAVF0300.Member_name );
            p_Extended_object_attribute                 = %trimr( SAVF0300.Extended_object_attribute );

            monitor;
              p_Save_date_and_time = RtvTimestamp( SAVF0300.Save_date_and_time );
            on-error *ALL;
              n_Save_date_and_time = PARM_NULL;
            endmon;

            p_Members_saved                             = SAVF0300.Members_saved;

            // Forward pointer to next entry.

            if ( CurEntry < UsrSpcHdr.NumLstEnt );
              ptrLstEnt += UsrSpcHdr.SizLstEnt;
            endif;

          // When last entry returned, signal EOF.

          when ( CurEntry >= UsrSpcHdr.NumLstEnt );
            SQL_State = '02000';
            return;

          other;
            SQL_State = '38999';
            MsgText = 'Unknown error reading savefile member info';
            return;
        endsl;

      when ( CallType = CALL_CLOSE );
        DltUsrSpc( QualUsrSpc : x'00000000' );
    endsl;

    // Normal return.

    return;

  // Error handling...

  on-error *all;
    RcvMsg( ApiError.ExcpID : MsgText );
    SQL_State = '38999';
    MsgText = 'Error ' + ApiError.ExcpID + ': ' + MsgText + '. Please check joblog.';
    return;
  endmon;

end-proc;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    Savefile_Spooled_Files_Info
// Description:  Return spooled files info from a savefile.

dcl-proc Savefile_Spooled_Files_Info export;

  dcl-pi *n;
    // Incoming parameters
    p_InSavefile                                           char( 10 ) const;
    p_InLibrary                                            char( 10 ) const;
    p_InObjNameFilter                                      char( 10 ) const;
    // Returned parameters
    p_Savefile                                             varchar( 10 );
    p_Savefile_Library                                     varchar( 10 );
    p_Job_name                                             varchar( 10 );
    p_User_name                                            varchar( 10 );
    p_Job_number                                           varchar(  6 );
    p_Spooled_file_name                                    varchar( 10 );
    p_Spooled_file_number                                  int( 10 );
    p_Job_system_name                                      varchar(  8 );
    p_Creation_date                                        date;
    p_Creation_time                                        time;
    p_Output_queue_name                                    varchar( 10 );
    p_Output_queue_library                                 varchar( 10 );
    // Null indicators for incoming parameters
    n_InSavefile                                           int( 5 ) const;
    n_InLibrary                                            int( 5 ) const;
    n_InObjNameFilter                                      int( 5 ) const;
    // Null indicators for returned parameters
    n_Savefile                                             int( 5 );
    n_Savefile_Library                                     int( 5 );
    n_Job_name                                             int( 5 );
    n_User_name                                            int( 5 );
    n_Job_number                                           int( 5 );
    n_Spooled_file_name                                    int( 5 );
    n_Spooled_file_number                                  int( 5 );
    n_Job_system_name                                      int( 5 );
    n_Creation_date                                        int( 5 );
    n_Creation_time                                        int( 5 );
    n_Output_queue_name                                    int( 5 );
    n_Output_queue_library                                 int( 5 );
    // SQL parameters.
    Sql_State   char( 5 );
    Function    varchar( 517 ) const;
    Specific    varchar( 128 ) const;
    MsgText     varchar( 1000 );
    CallType    int( 10 )      const;
  end-pi;

  dcl-s  InSavefile        like( p_InSavefile );
  dcl-s  InLibrary         like( p_InLibrary );
  dcl-s  InObjNameFilter   like( p_InObjNameFilter );

  // User space and pointers
  dcl-ds QualUsrSpc        likeds( QualObj_t ) static;
  dcl-s  ptrUsrSpc         pointer static inz( *null );
  dcl-s  ptrInpInf         pointer static inz( *null );
  dcl-s  ptrHdrInf         pointer static inz( *null );
  dcl-s  ptrLstEnt         pointer static inz( *null );
  dcl-ds UsrSpcHdr         likeds( UsrSpcHdr_t ) based( ptrUsrSpc );

  dcl-ds HEADER            likeds( HEADER_t ) based( ptrHdrInf );
  dcl-ds SAVF0400          likeds( SAVF0400_t ) based( ptrLstEnt );

  dcl-s  CurEntry          like( UsrSpcHdr.NumLstEnt ) static;

  dcl-ds ApiError          likeds( ApiError_t );

  // Global procedure monitor.

  monitor;

    // Start all fields at not NULL.

    n_Savefile                                             = PARM_NOTNULL;
    n_Savefile_Library                                     = PARM_NOTNULL;
    n_Job_name                                             = PARM_NOTNULL;
    n_User_name                                            = PARM_NOTNULL;
    n_Job_number                                           = PARM_NOTNULL;
    n_Spooled_file_name                                    = PARM_NOTNULL;
    n_Spooled_file_number                                  = PARM_NOTNULL;
    n_Job_system_name                                      = PARM_NOTNULL;
    n_Creation_date                                        = PARM_NOTNULL;
    n_Creation_time                                        = PARM_NOTNULL;
    n_Output_queue_name                                    = PARM_NOTNULL;
    n_Output_queue_library                                 = PARM_NOTNULL;

    //  Open, fetch & close...

    select;
      when ( CallType = CALL_OPEN );

        exec SQL values upper( :p_InSavefile ) into :InSavefile;
        exec SQL values upper( :p_InLibrary ) into :InLibrary;
        exec SQL values upper( :p_InObjNameFilter ) into :InObjNameFilter;

        if ( ( n_InSavefile = PARM_NULL ) or ( InSavefile = '' ) );
          SQL_State = '38999';
          MsgText = 'Savefile must be specified.';
          return;
        endif;

        if ( ( n_InLibrary = PARM_NULL ) or ( InLibrary = '' ) );
          SQL_State = '38999';
          MsgText = 'Library must be specified.';
          return;
        endif;

        if ( ( n_InObjNameFilter = PARM_NULL ) or ( InObjNameFilter = '' ) );
          SQL_State = '38999';
          MsgText = 'Object name filter must be specified.';
          return;
        endif;

        // Create temporary user space for program info list.

        QualUsrSpc.Obj = TEMP_USERSPACE + '4';
        QualUsrSpc.Lib = TEMP_USERSPACE_LIB;
        CrtUsrSpc( QualUsrSpc
                 : ''
                 : 65535
                 : x'00'
                 : '*CHANGE'
                 : %proc
                 : '*YES'
                 : x'00000000'
                 );
        ChgUsrSpcAttr( QualUsrSpc.Lib
                     : QualUsrSpc
                     : AttrsToChg
                     : x'00000000'
                     );

        // Get Savefile library info into user space.

        List_Savefile_Information( QualUsrSpc
                                 : 'SAVF0400'
                                 : InSavefile + InLibrary
                                 : InObjNameFilter
                                 : '*ALL'
                                 : ''
                                 : x'00000000'
                                 );

        GetPtrUsrSpc( QualUsrSpc : ptrUsrSpc );

        ptrInpInf = ptrUsrSpc + UsrSpcHdr.OfsInpSec;
        ptrHdrInf = ptrUsrSpc + UsrSpcHdr.OfsHdrSec;
        ptrLstEnt = ptrUsrSpc + UsrSpcHdr.OfsLstEnt;

        // Reset current entry before fetching.

        CurEntry = 0;

      when ( CallType = CALL_FETCH );

        // Read next list entry.

        select;
          when ( CurEntry < UsrSpcHdr.NumLstEnt );
            CurEntry += 1;

            // Copy savefile spooled files info to parameters.

            p_Savefile                                  = %trimr( HEADER.Save_file_name_used );
            p_Savefile_Library                          = %trimr( HEADER.Save_file_library_name_used );
            p_Job_name                                  = %trimr( SAVF0400.Job_name );
            p_User_name                                 = %trimr( SAVF0400.User_name );
            p_Job_number                                = %trimr( SAVF0400.Job_number );
            p_Spooled_file_name                         = %trimr( SAVF0400.Spooled_file_name );
            p_Spooled_file_number                       = SAVF0400.Spooled_file_number;
            p_Job_system_name                           = %trimr( SAVF0400.Job_system_name );
            p_Creation_date                             = %date( SAVF0400.Creation_date : *CYMD0 );
            p_Creation_time                             = %time( SAVF0400.Creation_time : *HMS0 );
            p_Output_queue_name                         = %trimr( SAVF0400.Output_queue_name );
            p_Output_queue_library                      = %trimr( SAVF0400.Output_queue_library );

            // Forward pointer to next entry.

            if ( CurEntry < UsrSpcHdr.NumLstEnt );
              ptrLstEnt += UsrSpcHdr.SizLstEnt;
            endif;

          // When last entry returned, signal EOF.

          when ( CurEntry >= UsrSpcHdr.NumLstEnt );
            SQL_State = '02000';
            return;

          other;
            SQL_State = '38999';
            MsgText = 'Unknown error reading savefile spooled files info';
            return;
        endsl;

      when ( CallType = CALL_CLOSE );
        DltUsrSpc( QualUsrSpc : x'00000000' );
    endsl;

    // Normal return.

    return;

  // Error handling...

  on-error *all;
    RcvMsg( ApiError.ExcpID : MsgText );
    SQL_State = '38999';
    MsgText = 'Error ' + ApiError.ExcpID + ': ' + MsgText + '. Please check joblog.';
    return;
  endmon;

end-proc;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    RtvTimestamp
// Description:  Retrieve ISO timestamp from system timestamp.

dcl-proc RtvTimestamp;

  dcl-pi *n timestamp;
    TS      like( Sys_timestamp_t );
  end-pi;

  dcl-pr Convert_Date_and_Time_Format extpgm( 'QWCCVTDT' );
    *n    char( 10 ) const;
    *n    char( 20 ) const options( *varsize );
    *n    char( 10 ) const;
    *n    char( 20 )       options (*varsize );
    *n    char(  8 ) const;
  end-pr;

  dcl-s  TS_yymd  char( 17 );

  if ( TS <> *blanks ) and
     ( TS <> *zeros );
    Convert_Date_and_Time_Format( '*DTS' : TS : '*YYMD' : TS_yymd : x'00000000' );
    return ( %date( %subst( TS_yymd : 1 : 8 ) : *ISO0 ) + %time( %subst( TS_yymd : 9 : 6 ) : *HMS0 ) );
  endif;

end-proc;

//-----------------------------------------------------------------------------------------------------------/
// Procedure:    RcvMsg
// Description:  Receive message.

dcl-proc RcvMsg;
  dcl-pi *n ind;
    MsgID     char( 7 );
    MsgText   varchar( 1000 );
  end-pi;

  dcl-pr QMHRCVPM extpgm( 'QMHRCVPM' );
    *n likeds( RCVM0200 )             options( *varsize );           // Receiver variable
    *n int( 10 )                      const;                         // Length of receiver variable
    *n char(    8 )                   const;                         // Format name
    *n char(   10 )                   const;                         // Call stack entry
    *n int( 10 )                      const;                         // Call stack counter
    *n char(   10 )                   const;                         // Message type
    *n char(    4 )                   const;                         // Message key
    *n int( 10 )                      const;                         // Wait time
    *n char(   10 )                   const;                         // Message action
    *n char( 1024 )                   options( *varsize );           // Error code
  end-pr;

  dcl-ds RCVM0200 qualified len( 4096 );
    Bytes_provided   int( 10 ) inz( %size( RCVM0200 ) );
    Bytes_available  int( 10 );
    Message_ID       char( 7 ) pos( 13 );
    Message_data_len int( 10 ) pos( 153 );
    Message_text_len int( 10 ) pos( 161 );
    Message_data     char( 1 ) pos( 177 );
  end-ds;

  dcl-s  Message_text        char( %size( MsgText ) ) based( ptrMessage_text );
  dcl-s  ptrMessage_text     pointer;

  dcl-ds ERRC0100 likeds( ApiError_t );

  // Receive last message.

  QMHRCVPM( RCVM0200 : %size( RCVM0200 ) : 'RCVM0200' : '*' : 1 : '*LAST' : '' : 0 : '*SAME' : ERRC0100 );

  if ( ERRC0100.BytAvl = 0 );
    MsgID = RCVM0200.Message_ID;
    ptrMessage_text = %addr( RCVM0200.Message_data ) + RCVM0200.Message_data_len;
    MsgText = %subst( Message_text : 1 : %min( %size( MsgText ) : RCVM0200.Message_text_len ) );
  endif;

  return ( ERRC0100.BytAvl = 0 );

end-proc;
