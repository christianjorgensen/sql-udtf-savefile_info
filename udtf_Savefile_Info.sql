-- Return Savefile information.

-- Build instructions:

-- *> RUNSQLSTM SRCSTMF('&FP') COMMIT(*NONE) DFTRDBCOL(&FCN2) <*

create or replace function Savefile_Library_Info (
                  Savefile                                   char( 10 )                       -- Savefile, generic, *ALL
                , Library                                    char( 10 )    default '*LIBL'    -- Library name, *LIBL etc.
                )
  returns table (                                            -- See List Save File (QSRLSAVF) API documentation for information about the information returned.
                  Savefile                                   varchar( 10 )
                , Savefile_library                           varchar( 10 )
                , Library_saved                              varchar( 10 )
                , Save_command                               varchar( 10 )
                , Save_date_and_time                         timestamp( 0 )
                , Auxiliary_storage_pool                     integer
                , Records                                    integer
                , Objects_saved                              integer
                , Access_paths                               integer
                , Save_active                                varchar( 10 )
                , Release_level                              varchar(  6 )
                , Data_compressed                            varchar(  3 )
                , System_serial_number                       varchar(  8 )
                , Private_authorities                        varchar(  3 )
                , Auxiliary_storage_pool_device_name         varchar( 10 )
                , Members_in_library_saved                   integer
                , Spooled_files_saved                        integer
                , Synchronization_ID                         varchar( 10 )
                )
  external name 'QGPL/SAVFINF(Savefile_Library_Info)'
  specific      SAVEFILE_LIBRARY_INFO
  language rpgle
  parameter style sql
  not deterministic
  no external action
  not fenced
  no scratchpad
  no final call
  disallow parallel
  cardinality 1
;

label on specific routine SAVEFILE_LIBRARY_INFO is 'Return savefile library info'
;

create or replace function Savefile_Object_Info (
                  Savefile                                   char( 10 )                       -- Savefile, generic, *ALL
                , Library                                    char( 10 )    default '*LIBL'    -- Library name, *LIBL etc.
                , Object_name_filter                         char( 10 )    default '*ALL'     -- Name, a generic name, or the special value *ALL.
                , Object_type_filter                         char( 10 )    default '*ALL'     -- The type of objects to search for or the special value *ALL.
                )
  returns table (                                            -- See List Save File (QSRLSAVF) API documentation for information about the information returned.
                  Savefile                                   varchar( 10 )
                , Savefile_library                           varchar( 10 )
                , Object_name                                varchar( 10 )
                , Library_saved                              varchar( 10 )
                , Object_type                                varchar( 10 )
                , Extended_object_attribute                  varchar( 10 )
                , Save_date_and_time                         timestamp( 0 )
                , Object_size                                bigint
                , Auxiliary_storage_pool                     integer
                , Data_saved                                 varchar(  3 )
                , Object_owner                               varchar( 10 )
                , Document_library_object_DLO_name           varchar( 20 )
                , Folder                                     varchar( 63 )
                , Text_description                           varchar( 50 )
                , Auxiliary_storage_pool_device_name         varchar( 10 )
                )
  external name 'QGPL/SAVFINF(Savefile_Object_Info)'
  specific      SAVEFILE_OBJECT_INFO
  language rpgle
  parameter style sql
  not deterministic
  no external action
  not fenced
  no scratchpad
  no final call
  disallow parallel
  cardinality 1
;

label on specific routine SAVEFILE_OBJECT_INFO is 'Return savefile object info'
;

create or replace function Savefile_Member_Info (
                  Savefile                                   char( 10 )                       -- Savefile, generic, *ALL
                , Library                                    char( 10 )    default '*LIBL'    -- Library name, *LIBL etc.
                , Object_name_filter                         char( 10 )    default '*ALL'     -- Name, a generic name, or the special value *ALL.
                )
  returns table (                                            -- See List Save File (QSRLSAVF) API documentation for information about the information returned.
                  Savefile                                   varchar( 10 )
                , Savefile_library                           varchar( 10 )
                , File_name                                  varchar( 10 )
                , Library_saved                              varchar( 10 )
                , Member_name                                varchar( 10 )
                , Extended_object_attribute                  varchar( 10 )
                , Save_date_and_time                         timestamp(0)
                , Members_saved                              integer
                )
  external name 'QGPL/SAVFINF(Savefile_Member_Info)'
  specific      SAVEFILE_MEMBER_INFO
  language rpgle
  parameter style sql
  not deterministic
  no external action
  not fenced
  no scratchpad
  no final call
  disallow parallel
  cardinality 1
;

label on specific routine SAVEFILE_MEMBER_INFO is 'Return savefile member info'
;

create or replace function Savefile_Spooled_Files_Info (
                  Savefile                                   char( 10 )                       -- Savefile, generic, *ALL
                , Library                                    char( 10 )    default '*LIBL'    -- Library name, *LIBL etc.
                , Object_name_filter                         char( 10 )    default '*ALL'     -- Name, a generic name, or the special value *ALL.
                )
  returns table (                                            -- See List Save File (QSRLSAVF) API documentation for information about the information returned.
                  Savefile                                   varchar( 10 )
                , Savefile_library                           varchar( 10 )
                , Job_name                                   varchar( 10 )
                , User_name                                  varchar( 10 )
                , Job_number                                 varchar(  6 )
                , Spooled_file_name                          varchar( 10 )
                , Spooled_file_number                        integer
                , Job_system_name                            varchar(  8 )
                , Creation_date                              date
                , Creation_time                              time
                , Output_queue_name                          varchar( 10 )
                , Output_queue_library                       varchar( 10 )
                )
  external name 'QGPL/SAVFINF(Savefile_Spooled_Files_Info)'
  specific      SAVEFILE_SPOOLED_FILES_INFO
  language rpgle
  parameter style sql
  not deterministic
  no external action
  not fenced
  no scratchpad
  no final call
  disallow parallel
  cardinality 1
;

label on specific routine SAVEFILE_SPOOLED_FILES_INFO is 'Return savefile spooled files info'
;

create or replace function Savefile_Info (
                  Savefile                                   char( 10 )                       -- Savefile, generic, *ALL
                , Library                                    char( 10 )    default '*LIBL'    -- Library name, *LIBL etc.
                , Object_name_filter                         char( 10 )    default '*ALL'     -- Name, a generic name, or the special value *ALL.
                , Object_type_filter                         char( 10 )    default '*ALL'     -- The type of objects to search for or the special value *ALL.
                )
  returns table (
                -- Unique columns from UDTF Savefile_Library_Info
                  Savefile                                   varchar( 10 )
                , Savefile_library                           varchar( 10 )
                , Library_saved                              varchar( 10 )
                , Save_command                               varchar( 10 )
                , Save_date_and_time                         timestamp( 0 )
                , Auxiliary_storage_pool                     integer
                , Records                                    integer
                , Objects_saved                              integer
                , Access_paths                               integer
                , Save_active                                varchar( 10 )
                , Release_level                              varchar(  6 )
                , Data_compressed                            varchar(  3 )
                , System_serial_number                       varchar(  8 )
                , Private_authorities                        varchar(  3 )
                , Auxiliary_storage_pool_device_name         varchar( 10 )
                , Members_in_library_saved                   integer
                , Spooled_files_saved                        integer
                , Synchronization_ID                         varchar( 10 )
                -- Unique columns from UDTF Savefile_Object_Info
                , Object_name                                varchar( 10 )
                , Object_type                                varchar( 10 )
                , Extended_object_attribute                  varchar( 10 )
                , Object_size                                bigint
                , Data_saved                                 varchar(  3 )
                , Object_owner                               varchar( 10 )
                , Document_library_object_DLO_name           varchar( 20 )
                , Folder                                     varchar( 63 )
                , Text_description                           varchar( 50 )
                -- Unique columns from UDTF Savefile_Member_Info
                , Member_name                                varchar( 10 )
                , Members_saved                              integer
                -- Unique columns from UDTF Savefile_Spooled_Files_Info
                , Job_name                                   varchar( 10 )
                , User_name                                  varchar( 10 )
                , Job_number                                 varchar(  6 )
                , Spooled_file_name                          varchar( 10 )
                , Spooled_file_number                        integer
                , Job_system_name                            varchar(  8 )
                , Creation_date                              date
                , Creation_time                              time
                )
  specific      SAVEFILE_INFO
  language SQL
  modifies SQL data
  not fenced
  set option commit = *none, dbgview = *source

  begin
    return
           with Lib  as ( select *
                            from table( Savefile_Library_Info       ( Savefile, Library ) )
                        )
              , Obj  as ( select *
                            from table( Savefile_Object_Info        ( Savefile, Library, Object_Name_Filter, Object_Type_Filter ) )
                        )
              , Mbr  as ( select *
                            from table( Savefile_Member_Info        ( Savefile, Library, Object_Name_Filter ) )
                        )
              , SplF as ( select *
                            from table( Savefile_Spooled_Files_Info ( Savefile, Library, Object_Name_Filter ) )
                        )
           select
                -- Unique columns from UDTF Savefile_Library_Info
                  Lib.Savefile
                , Lib.Savefile_Library
                , Lib.Library_saved
                , Lib.Save_command
                , Lib.Save_date_and_time
                , Lib.Auxiliary_storage_pool
                , Lib.Records
                , Lib.Objects_saved
                , Lib.Access_paths
                , Lib.Save_active
                , Lib.Release_level
                , Lib.Data_compressed
                , Lib.System_serial_number
                , Lib.Private_authorities
                , Lib.Auxiliary_storage_pool_device_name
                , Lib.Members_in_library_saved
                , Lib.Spooled_files_saved
                , Lib.Synchronization_ID
                -- Unique columns from UDTF Savefile_Object_Info
                , Obj.Object_name
                , Obj.Object_type
                , Obj.Extended_object_attribute
                , Obj.Object_size
                , Obj.Data_saved
                , Obj.Object_owner
                , Obj.Document_library_object_DLO_name
                , Obj.Folder
                , Obj.Text_description
                -- Unique columns from UDTF Savefile_Member_Info
                , Mbr.Member_name
                , Mbr.Members_saved
                -- Unique columns from UDTF Savefile_Spooled_Files_Info
                , SplF.Job_name
                , SplF.User_name
                , SplF.Job_number
                , SplF.Spooled_file_name
                , SplF.Spooled_file_number
                , SplF.Job_system_name
                , SplF.Creation_date
                , SplF.Creation_time
             from Lib left outer join Obj  on ( Lib.Savefile, Lib.Savefile_library ) =
                                              ( Obj.Savefile, Obj.Savefile_library )
                      left outer join Mbr  on ( Obj.Savefile, Obj.Savefile_library, Obj.Object_name, Obj.Object_type ) =
                                              ( Mbr.Savefile, Mbr.Savefile_library, Mbr.File_name,   '*FILE' )
                      left outer join SplF on ( Obj.Savefile,  Obj.Savefile_library,  Obj.Object_name,        Obj.Object_type ) =
                                              ( SplF.Savefile, SplF.Savefile_library, SplF.Output_queue_name, '*OUTQ' )
      ;
  end
;

label on specific routine SAVEFILE_INFO is 'Return savefile info'
;
